﻿using System.Net;
namespace IPv4LogFilter;

internal record LogEntry(DateTime RequestDateTime, IPAddress Address);

