﻿using System.Net;
using System.Net.Sockets;

namespace IPv4LogFilter
{
    public static class Extensions
    {
        public static int AsInt(this IPAddress address)
        {
            var ipAddressAsBytes = address.GetAddressBytes();
            Array.Reverse(ipAddressAsBytes);
            var ipAddressAsInt = BitConverter.ToInt32(ipAddressAsBytes);
            return ipAddressAsInt;
        }

        public static IPAddress? GetBroadcastAddress(this IPAddress address, IPAddress? mask)
        {
            if (mask == null)
                return null;
            var maskAsInt = mask.AsInt();
            var ipAddressAsInt = address.AsInt();

            var maskAsIntInverse = ~maskAsInt;
            var endAddressAsInt = ipAddressAsInt | maskAsIntInverse;

            var endAdrresAsBytes = BitConverter.GetBytes(endAddressAsInt);
            Array.Reverse(endAdrresAsBytes);
            var endAddress = new IPAddress(endAdrresAsBytes);

            return endAddress;
        }


        public static bool ValidMask(this IPAddress mask)
        {
            // Если маска не для IPv4 адреса, значит не валидна в рамках задачи
            if (mask.AddressFamily != AddressFamily.InterNetwork)
                return false;
            // Проверяем на валидность каждый октет маски. А так же то, что после октета,
            // меньшего чем 255, остальные должны быть 0
            byte[] validOctets = [255, 254, 252, 248, 240, 224, 192, 128, 0];
            byte[] ipOctets = mask.GetAddressBytes();
            bool restAreZeros = false;
            for (int i = 0; i < 4; i++)
            {
                if (!validOctets.Contains(ipOctets[i]))
                    return false;
                if (restAreZeros && ipOctets[i] != 0)
                    return false;
                if (ipOctets[i] < 255)
                    restAreZeros = true;
            }
            return true;
        }
    }
}
