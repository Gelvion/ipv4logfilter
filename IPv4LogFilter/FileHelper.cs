﻿namespace IPv4LogFilter
{
    internal static class FileHelper
    {
        public static void TryDeleteFile(string filePath)
        {
            try
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
            }
            catch { }
        }
    }
}
