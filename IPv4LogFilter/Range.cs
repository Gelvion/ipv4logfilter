﻿namespace IPv4LogFilter
{
    public class Range<T>(T start, T end)
    {
        public T Start { get; set; } = start;
        public T End { get; set; } = end;
    }
}
