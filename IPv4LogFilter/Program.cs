﻿
using IPv4LogFilter;
using System.Globalization;
using System.Net;

Console.WriteLine("Hello, User!");
Console.WriteLine();

var configData = Config.Data;

string? logFilePath = configData.FileLog;
if (logFilePath == null)
{
    Console.WriteLine("Необходимо запустить утилиту с указанием праметра file-log через аргументы командной строки или файл appsetting.json");
    Console.ReadLine();
    return;
}

string? outputFilePath = configData.FileOutput ?? "OutputIp.txt";

//Парсим входные параметры
string? addressStartAsString = configData.AddressStart;
IPAddress? addressStart = null;
if (addressStartAsString != null)
    IPAddress.TryParse(addressStartAsString, out addressStart);

string? addressMaskAsString = configData.AddressMask;
IPAddress? addressMask = null;
if (addressStart != null)
{
    if (IPAddress.TryParse(addressMaskAsString, out IPAddress? addressMaskForCheck))
    {
        if (addressMaskForCheck.ValidMask())
            addressMask = addressMaskForCheck;
    }
}

IPAddress? addressEnd = null;
if (addressStart != null)
    addressEnd = addressStart.GetBroadcastAddress(addressMask);

DateTime? dateTimeStart = null;
if (DateTime.TryParseExact(configData.TimeStart, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime timeStart))
    dateTimeStart = timeStart;

DateTime? dateTimeEnd = null;
if (DateTime.TryParseExact(configData.TimeEnd, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime timeEnd))
    dateTimeEnd = timeEnd;

var adressesRange = new Range<IPAddress?>(addressStart, addressEnd);
var dateTimesRange = new Range<DateTime?>(dateTimeStart, dateTimeEnd);

var ipFilterManager = new IpFilterManager();
try
{
    await ipFilterManager.SaveAddressesAsync(adressesRange, dateTimesRange, logFilePath, outputFilePath);
    Console.WriteLine("Отфильтрованные адреса успешно сохранены");
}
 //Отлов ексепшенов на верхнем уровне
catch (FileNotFoundException e)
{
    Console.WriteLine($"Указанный фай с логоv не найден: {e.FileName}");
}
catch (UnauthorizedAccessException e)
{
    Console.WriteLine($"Отказано в доуступе к файлу");
Console.WriteLine($"Попробуйте перезапустите утилиту с правами администратора");
}
catch (PathTooLongException e)
{
    Console.WriteLine($"Путь к файлу слишком большой длины");
Console.WriteLine(e.Message);
}
catch (Exception e)
{
    if ((e is FormatException) || (e is IndexOutOfRangeException))
    {
        Console.WriteLine("Неверный формат файла с логом");
        //Если входной файл не парситься, пытаемся удалить неполный выходной
        FileHelper.TryDeleteFile(outputFilePath);
    }
    else
{
    Console.WriteLine("Что-то пошло не так");
    Console.WriteLine(e.Message);
}
}

Console.ReadLine();
