﻿using Microsoft.Extensions.Configuration;

namespace IPv4LogFilter
{
    internal class Config
    {
        public static ConfigData Data
        {
            get 
            {
                var environment = Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT");

                var builder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json", optional:true);

                if (environment != null && environment.Equals("dev", StringComparison.InvariantCultureIgnoreCase))
                    builder = builder.AddJsonFile("appsettings.dev.json", optional:true);

                builder.AddCommandLine(Environment.GetCommandLineArgs());

                var config = builder.Build();

                return config.Get<ConfigData>()!;
            }
        }

    }
}
