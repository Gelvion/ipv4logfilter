﻿using Microsoft.Extensions.Configuration;
using System.Net;

namespace IPv4LogFilter
{
    public class ConfigData
    {
        [ConfigurationKeyName("file-log")]
        public string? FileLog { get; set; }

        [ConfigurationKeyName("file-output")]
        public string? FileOutput { get; set;}

        [ConfigurationKeyName("address-start")]
        public string? AddressStart { get; set; }

        [ConfigurationKeyName("address-mask")]
        public string? AddressMask { get; set; }

        [ConfigurationKeyName("time-start")]
        public string? TimeStart { get; set; }

        [ConfigurationKeyName("time-end")]
        public string? TimeEnd { get; set; }

    }
}