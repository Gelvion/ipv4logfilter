﻿using System.Collections.Concurrent;
using System.Globalization;
using System.Net;

namespace IPv4LogFilter
{
    public class IpFilterManager
    {
        private  BlockingCollection<IPAddress> _blockingCollection = new();
        public async Task SaveAddressesAsync(Range<IPAddress?> adressesRange, Range<DateTime?> dateTimesRange, string logFilePath, string ouputFilePath)
        {
            // Реализация паттерна producer/consumer
            // Один поток (producer) читает и фильтрует даннные из входного файла
            // Другой поток (consumer) формирует данные и пишет их в выходной файл

            _blockingCollection = new BlockingCollection<IPAddress>();

            var produceTask = Task.Run(() =>
            {
                var logEntries = ReadLogEntries(logFilePath);
                var filteredAddresses = GetFilteredAddresses(logEntries, adressesRange, dateTimesRange).OrderBy(a => a.AsInt());

                try
                {
                    foreach (var address in filteredAddresses)
                    {
                        _blockingCollection.Add(address);
                    }
                }
                finally
                {
                    _blockingCollection.CompleteAdding();
                }
            });
            var consumeTask = Task.Run(() => SaveAddresses(ouputFilePath));

            await Task.WhenAll(produceTask, consumeTask);
        }

        private IEnumerable<LogEntry> ReadLogEntries(string logFilePath)
        {
            using var stream = new FileStream(logFilePath, FileMode.Open);
            using var reader = new StreamReader(stream);
            string? line;
            string[] data;
            while ((line = reader.ReadLine()) != null)
            {
                data = line.Split(":", 2);
                var address = IPAddress.Parse(data[0]);
                var dateTime = DateTime.ParseExact(data[1], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                yield return new LogEntry(dateTime, address);
            }
        }

        private IEnumerable<IPAddress> GetFilteredAddresses(IEnumerable<LogEntry> logEntries, Range<IPAddress?> adressesRange, Range<DateTime?> dateTimesRange)
        {
            if (adressesRange.Start != null)
                logEntries = logEntries.Where(e => e.Address.AsInt() >= adressesRange.Start.AsInt());

            if (adressesRange.End != null)
                logEntries = logEntries.Where(e => e.Address.AsInt() < adressesRange.End.AsInt());

            if (dateTimesRange.Start != null)
                logEntries = logEntries.Where(e => e.RequestDateTime >= dateTimesRange.Start);

            if (dateTimesRange.End != null)
                logEntries = logEntries.Where(e => e.RequestDateTime <= dateTimesRange.End);

            return logEntries.Select(e => e.Address);
        }

        private void SaveAddresses(string ouputFilePath)
        {
            var groupedAddresses = _blockingCollection.GetConsumingEnumerable()
                .GroupBy(a => a)
                .Select(g => new
                {
                    Address = g.Key,
                    RequestCount = g.Count()
                })
                .OrderByDescending(g => g.RequestCount);

            using var stream = new FileStream(ouputFilePath, FileMode.Create);
            using var writer = new StreamWriter(stream);
            foreach (var gAddress in groupedAddresses)
            {
                writer.WriteLine($"{gAddress.Address} {gAddress.RequestCount}");
            }
        }
    }
}
