using System.Net;
using System;

namespace IPv4LogFilter.Tests
{
    [TestClass]
    public class IpFilterManagerTests
    {
        private readonly static IpFilterManager _manager = new();

        [TestMethod]
        [DataRow(null, null, null, null, 1)]
        [DataRow("10.04.2024", null, null, null, 2)]
        [DataRow("10.04.2024", "13.04.2024", null, null, 3)]
        [DataRow("10.04.2024", "13.04.2024", "192.168.125.58", null, 4)]
        [DataRow("10.04.2024", "13.04.2024", "192.168.125.58", "192.168.125.127", 5)]
        [DeploymentItem("Files\\")]
        public async Task SaveAddressesAsync_ValuesFromDataRow(string? dateStartString, string? dateEndString, string? addressStartString, string? addressEndString, int fileNum)
        {
            // arrange
            DateTime? dateStart = (dateStartString != null) ? DateTime.Parse(dateStartString) : null;
            DateTime? dateEnd = (dateEndString != null) ? DateTime.Parse(dateEndString) : null;
            IPAddress? addressStart = (addressStartString != null) ? IPAddress.Parse(addressStartString) : null;
            IPAddress? addressEnd = (addressEndString != null) ? IPAddress.Parse(addressEndString) : null;

            var adressesRange = new Range<IPAddress?>(addressStart, addressEnd);
            var dateTimesRange = new Range<DateTime?>(dateStart, dateEnd);
            var expectedResult = File.ReadAllText($"ResultTemplate{fileNum}.txt");
            
            // act
            await _manager.SaveAddressesAsync(adressesRange, dateTimesRange, "IpLogSmall.txt", "IpOutput.txt");
            var actualResult = File.ReadAllText($"IpOutput.txt");

            // assert
            Assert.AreEqual(actualResult, expectedResult);

        }
    }
}