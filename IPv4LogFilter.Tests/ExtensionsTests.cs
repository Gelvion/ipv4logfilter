﻿using System.Net;

namespace IPv4LogFilter.Tests
{
    [TestClass]
    public class ExtensionsTests
    {
        [TestMethod]
        public void GetBroadcast_AddressAndMask_ReturnBroadcastIp()
        {
            // arrange
            var address = IPAddress.Parse("192.168.125.58");
            var mask = IPAddress.Parse("255.255.255.128");
            var expected = IPAddress.Parse("192.168.125.127");
            // act
            var broadcastAddress = address.GetBroadcastAddress(mask);
            // assert
            Assert.AreEqual(expected, broadcastAddress);
        }

        [TestMethod]
        public void ValidMask_ValidMask_ReturnTrue()
        {
            // arrange
            var mask = IPAddress.Parse("255.255.255.128");
            var expected = true;
            // act
            var valid = mask.ValidMask();
            // assert
            Assert.AreEqual(expected, valid);
        }

        [TestMethod]
        public void ValidMask_InvalidMask_ReturnFalse()
        {
            // arrange
            var mask = IPAddress.Parse("255.255.255.100");
            var expected = false;
            // act
            var valid = mask.ValidMask();
            // assert
            Assert.AreEqual(expected, valid);
        }

        [TestMethod]
        public void ValidMask_IPv6Mask_ReturnFalse()
        {
            // arrange
            var mask = IPAddress.Parse("ffff:ffff:ffff:fff0::");
            var expected = false;
            // act
            var valid = mask.ValidMask();
            // assert
            Assert.AreEqual(expected, valid);
        }
    }
}
